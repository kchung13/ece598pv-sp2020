use serde::{Serialize,Deserialize};
use ring::signature::{self, Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters};
extern crate rand;
use rand::Rng;

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Transaction {
    input: f32, //float 32
    output: f32,

}

pub struct SignedTransaction {
    transaction: Transaction,
    signature: Signature,

}

/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
   
    let s_transaction: Vec<u8> = bincode::serialize(&t).unwrap();
    let signature = key.sign(&s_transaction);

    return signature;
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &<Ed25519KeyPair as KeyPair>::PublicKey, signature: &Signature) -> bool {
    let peer_public_key_bytes = public_key.as_ref();
    let s_transaction: Vec<u8> = bincode::serialize(&t).unwrap();
    let peer_public_key = signature::UnparsedPublicKey::new(&signature::ED25519, peer_public_key_bytes);
    let verify_output = match peer_public_key.verify(&s_transaction, signature.as_ref()) {
    Ok(verify_output)  => true,
    Err(verify_output) => false,
    };
    return verify_output;
    
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::crypto::key_pair;

    pub fn generate_random_transaction() -> Transaction {
        let mut rng = rand::thread_rng();
        return Transaction{ input: rng.gen::<f32>(), output: rng.gen::<f32>()}
    }

    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
    }
}
