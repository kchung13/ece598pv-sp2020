
use super::hash::{Hashable, H256};

/// A Merkle tree.
#[derive(Debug, Default)]
pub struct MerkleTree {
    root: H256, 
    tree: Vec<H256>,
}

pub struct Leaf {
        hash: H256,
        pair_hash: H256,
    }


impl MerkleTree {
    pub fn new<T>(data: &[T]) -> Self where T: Hashable, {
        let data_count = data.len();
        let mut stack_c = Vec::new();
        let mut temp = Vec::new();

        for d in data {
            let leaf = d.hash();
            stack_c.push(leaf);
        }

        while stack_c.len() > 1 {
            let mut stack_n = Vec::new();
            while !stack_c.is_empty() {
                if stack_c.len() == 1 {
                    stack_c.push(stack_c[0]); //odd number of nodes, duplicate
                } 
                let left = stack_c.remove(0);
                let right = stack_c.remove(0);
                temp.insert(0, left);
                temp.insert(0, right);
                let mut context = ring::digest::Context::new(&ring::digest::SHA256);
                context.update(left.as_ref());
                context.update(right.as_ref());
                let combined_H256 = <H256>::from(context.finish());

                stack_n.push(combined_H256);
            
            }

        stack_c = stack_n;
        }
        let root = stack_c.remove(0);

         return MerkleTree {
            root,
            tree : temp,
        }

    }

    pub fn root(&self) -> H256 {
        return self.root;

    }

    /// Returns the Merkle Proof of data at index i
    // does not include root or the data at that index
    pub fn proof(&self, index: usize) -> Vec<H256> {
        let mut indices = Vec::new();
        let mut proof = Vec::new();
        let mut curr_index = index;
        let mut next_index = index;
        indices.push(index); //if index is 0
        //get indices of path
        while curr_index != 0 {
            next_index = (curr_index/2)-1;
            indices.push(next_index)
          /*  if curr_index %2 == 0 {
                next_index = (curr_index/2) - 1;
                indices.push(next_index);
            }
            else {
                next_index = curr_index/2;
                indices.push(next_index);
            }*/
        }
        while !indices.is_empty() {
            curr_index = indices.remove(0);
            proof.push(self.tree[curr_index]);
        }
    //    indices.push(0); 
        // let prev_hash = self.root;
        // while !indices.is_empty() {
        //     curr_index = indices.remove(0);
        //     //&self.0[0], &self.0[1], &self.0[30], &self.0[31]
        // //    let byte_hash = <[u8; 32]>::from(&prev_hash);
        //     let byte_hash = prev_hash.as_ref();
        //     //digest::digest(&digest::SHA256, b"hello, world")
        //     let left_byte = &byte_hash[0..15];
        //     let right_byte = &byte_hash[16..31];
        //     //push right and left because need both hashes
        //     let mut contextRight = ring::digest::Context::new(&ring::digest::SHA256);
        //     contextRight.update(&right_byte);
        //     proof.push(<H256>::from(contextRight.finish()));
        //     // let mut contextLeft = ring::digest::Context::new(&ring::digest::SHA256);
        //     // contextLeft.update(&left_byte);
        //     // proof.push(<H256>::from(contextLeft.finish()));
            
        //     //even index is right child
        //     if curr_index % 2 == 0 {
                
        //     }
        //     //odd index is left child
        //     else {
                
        //     }
        // }
         return proof;

    }
}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], index: usize, leaf_size: usize) -> bool {
    let mut curr_hash = datum;
    let mut curr_index = index;
    let mut stack_hash = Vec::new();
    stack_hash.push(*curr_hash);
    for x in proof  {
        //even = left side
        if curr_index%2 == 0 {
            let left_byte = stack_hash.remove(0);
            let right_byte = x;
            let mut context = ring::digest::Context::new(&ring::digest::SHA256);
            context.update(left_byte.as_ref());
            context.update(right_byte.as_ref());
            let hashed = <H256>::from(context.finish());
            stack_hash.push(hashed);
            if curr_index != 0 {
                curr_index = (curr_index/2) - 1;
            }
            
        }
        //odd = right side
        else {
            let right_byte = stack_hash.remove(0);
            let left_byte = x;
            let mut context = ring::digest::Context::new(&ring::digest::SHA256);
            context.update(left_byte.as_ref());
            context.update(right_byte.as_ref());
            let hashed = <H256>::from(context.finish());
            stack_hash.push(hashed);
            if curr_index != 0 {
                curr_index = (curr_index/2) - 1;
            }

        }

        if stack_hash.remove(0) == *root {
        return true;
    }
    
    }
   

    return false;
}

#[cfg(test)]
mod tests {
    use crate::crypto::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }

    #[test]
    fn root() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into()
        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }
    /*
        #[test]
    fn proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }*/

    #[test]
    fn verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert!(verify(&merkle_tree.root(), &input_data[0].hash(), &proof, 0, input_data.len()));
    }
/*
    #[test]
    fn verifyingFail() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(2);
        assert_ne!(verify(&merkle_tree.root(), &input_data[0].hash(), &proof, 0, input_data.len()));
    }*/
}
